import React, {Component, useState, useEffect} from 'react'
//import './UserProfile.scss';
import CreatableSelect from 'react-select/creatable';
import http from "../../config/http"

const scaryAnimals = [
  { label: "Alligators", value: 1 },
  { label: "Crocodiles", value: 2 },
  { label: "Sharks", value: 3 },
  { label: "Small crocodiles", value: 4 },
  { label: "Smallest crocodiles", value: 5 },
  { label: "Snakes", value: 6 },
];

const customStyles = {
    option: (provided: any, state: { isSelected: any; }) => ({
      ...provided,
      borderBottom: '1px dotted pink',
      padding: 10,
    }),
    control: () => ({
        backgroundColor: "transparent",
        color:"red",
    }),
      singleValue: (provided: any, state: { isDisabled: any; }) => {
        const opacity = state.isDisabled ? 0.5 : 1;
        const transition = 'opacity 300ms';
    
        return { ...provided, opacity, transition };
      }
    }
    

class CreatableSingle extends Component {
    handleChange = (newValue: any, actionMeta: any) => {
      console.group('Value Changed');
      console.log(newValue);
      console.log(`action: ${actionMeta.action}`);
      console.groupEnd();
    };
    handleInputChange = (inputValue: any, actionMeta: any) => {
      console.group('Input Changed');
      console.log(inputValue);
      console.log(`action: ${actionMeta.action}`);
      console.groupEnd();
    };
    render() {
      return (
        <CreatableSelect
          isClearable
          styles={customStyles}
          options={scaryAnimals}
          onChange={this.handleChange}
          onInputChange={this.handleInputChange}
        />
      );
    }
  }

export default () => {

    interface IUpdateProfile {
      name: string;
      institutionId: number;
      institutionName: string,
      gender: string;
      mobileNumber: string;
      category: string;
    }
  
    const [updateProfile, setupdateProfile] = useState<IUpdateProfile>({
      name: 'Alec Thomson',
      institutionId: 432,
      institutionName: 'MEC',
      gender: 'Male',
      mobileNumber: '9273547383',
      category: 'B'
    });
  
    interface IViewProfile {
      id: number;
      name: string;
      email: string;
      picture: string;
      qrCodeUrl: string;
      institutionName: string,
      gender: string;
      mobileNumber: string;
      category: string;
    }
  
    const [viewProfile, setviewProfile] = useState<IViewProfile>({
      id: 11042020,
      name: 'Alec Thomson',
      email: 'alecthomson@gmail.com',
      picture: 'xyz',
      qrCodeUrl: 'xyz',
      institutionName: 'MEC',
      gender: 'Male',
      mobileNumber: '9273547383',
      category: 'B'
    });
/*
    interface ICollegeList [
      {
        "id": number;
        "name": string;
      }
    ]
  
    const [collegeList, setcollegeList] = useState<ICollegeList>([
      {
        "id": 0,
        "name": "string"
      }
    ]);
  */
    const [editable, seteditable] = useState<boolean>(false);
    var opts = {readOnly:!editable};
  
    const handleUpdateProfile = (event: { target: { name: any; value: any; }; }) => {
      {
      const { name, value } = event.target;
      setupdateProfile({ ...updateProfile, [ name]: value });
      }
    };
  
    const handleEdit = (event: { preventDefault: () => void; }) => {
      seteditable(true);
      opts = {readOnly:!editable};
      event.preventDefault();
    }
   
  const submitValue = (event: { preventDefault: () => void; }) => {
          seteditable(false);
          opts = {readOnly:true};
          event.preventDefault();
  
      http.post('/Profile/update', updateProfile)

     // console.log(updateProfile);
     // console.log(viewProfile);
  }

  useEffect(() => {
    // GET request using fetch inside useEffect React hook
   // console.log(updateProfile)

    http.get('/Profile/view')
    .then((res: any) => console.log(res));
     
     //console.log(updateProfile)

/*
     http.get('/Profile/view')
    .then(response => setviewProfile({
      id: response.id,
      name: response.name,
      email: response.email,
      picture: response.picture,
      qrCodeUrl: response.qrCodeUrl,
      institutionName: response.institutionName,
      gender: response.gender,
      mobileNumber: response.mobileNumber,
      category: response.category
     }));
*/
}, []);
  
  return(
      <>
          <div className="content">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-8">
                <div className="card">
                  <div className="card-header card-header-primary">
                    <h4 className="card-title">Edit Profile</h4>
                    <p className="card-category">Complete your profile</p>
                  </div>
                  <div className="card-body">
                    <form>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="form-group">
                            <label className="bmd-label-floating">Name</label>
                            <input type="text" className="form-control" name="name" value={updateProfile.name} onChange={handleUpdateProfile} required {...opts} style={{'backgroundColor':'transparent'}}/>
                          </div>
                        </div>
                        <div className="col-md-6">
                        <div className="form-group">
                            <label className="bmd-label-floating">Gender</label>
                            <input type="text" className="form-control"  name="gender" value={updateProfile.gender} onChange={handleUpdateProfile} {...opts} style={{'backgroundColor':'transparent'}} />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label className="bmd-label-floating">Institution Name</label>
                            <select className="form-control"  name="institutionName" value={updateProfile.institutionName} onChange={handleUpdateProfile} {...opts} style={{'backgroundColor':'transparent'}}>
                           </select>
                            <CreatableSingle />

                          </div>
                        </div>
                        <div className="col-md-6">
                          
                        <div className="form-group">
                            <label className="bmd-label-floating">Institution ID</label>
                            <input type="text" className="form-control"  name="institutionId" value={updateProfile.institutionId} onChange={handleUpdateProfile}  {...opts} style={{'backgroundColor':'transparent'}} />
                          </div>
                        </div>
                        </div>
                        <div className="row">
                        <div className="col-md-6">
                          <div className="form-group">
                            <label className="bmd-label-floating">Mobile Number</label>
                            <input type="text" className="form-control"  name="mobileNumber" value={updateProfile.mobileNumber} onChange={handleUpdateProfile}  {...opts} style={{'backgroundColor':'transparent'}} />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label className="bmd-label-floating">Category</label>
                            <input type="text" className="form-control"  name="category" value={updateProfile.category} onChange={handleUpdateProfile}  {...opts} style={{'backgroundColor':'transparent'}}/>
                          </div>
                        </div>
                      </div>
                    
                      <button type="submit" onClick={handleEdit} className="btn btn-primary pull-right">Edit Profile</button>
                      <button type="submit" onClick={submitValue} className="btn btn-primary pull-right">Update Profile</button>
                      <div className="clearfix"></div>
                    </form>
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div className="card card-profile">
                  <div className="card-avatar">
                    <a href="#pablo">
                      <img className="img" src="../assets/img/faces/marc.jpg" /> {/* replace src={viewProfile.picture} */}
                    </a>
                  </div>
                  <div className="card-body">
                    <h6 className="card-category">{viewProfile.email}</h6>
                    <h4 className="card-title" style={{'marginBottom':'2vh'}}>{viewProfile.name}</h4>
                    <p className="card-description">Excel ID: {viewProfile.id}</p>
                    <p className="card-description">QR Code URL: {viewProfile.qrCodeUrl}</p>
                    <p className="card-description">Institution Name: {viewProfile.institutionName}</p>
                    <p className="card-description">Gender: {viewProfile.gender}</p>
                    <p className="card-description">Mobile Number: {viewProfile.mobileNumber}</p>
                    <p className="card-description">Category: {viewProfile.category}</p>
  
                    {/*<a href="#pablo" className="btn btn-primary btn-round">Follow</a>*/}
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
      </>
      )
  }