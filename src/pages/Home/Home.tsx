import React, { useEffect, lazy, Suspense } from 'react';

import http from '../../config/http';
import DottedLineLoader from '../../components/common/Loaders/DottedLineLoader';
import './Home.scss';

const RegEvCard = lazy(() => import('../../components/RegisteredEvents/RegEvCard'));
const Table = lazy(() => import('../../components/RegisteredEvents/Table'));

const d1 = [
    {
        id: '1',
        name: 'Kryptos',
        date: '03/01/2020',
        link: 'https://excelplay.org',
    },
    {
        id: '2',
        name: 'Dalal Bull',
        date: '04/01/2020',
        link: 'https://excelplay.org',
    },
]

const d2 = [
    {
        id: '1',
        name: 'Kryptos',
        date: '03/01/2020',
        link: 'https://excelplay.org',
    },
    {
        id: '2',
        name: 'Dalal Bull',
        date: '04/01/2020',
        link: 'https://excelplay.org',
    },
    {
        id: '3',
        name: 'Algorithms',
        date: '05/02/2020',
        link: 'https://excelplay.org',
    },
    {
        id: '4',
        name: 'Kryptos',
        date: '03/01/2020',
        link: 'https://excelplay.org',
    },
    {
        id: '5',
        name: 'Dalal Bull',
        date: '04/01/2020',
        link: 'https://excelplay.org',
    },
    {
        id: '6',
        name: 'Algorithms',
        date: '05/02/2020',
        link: 'https://excelplay.org',
    },
    {
        id: '7',
        name: 'Kryptos',
        date: '03/01/2020',
        link: 'https://excelplay.org',
    },
    {
        id: '8',
        name: 'Dalal Bull',
        date: '04/01/2020',
        link: 'https://excelplay.org',
    }
]

const d3 = [
    {
        id: '1',
        name: 'Kryptos',
        date: '03/01/2020',
        link: 'https://excelplay.org',
    },
    {
        id: '2',
        name: 'Dalal Bull',
        date: '04/01/2020',
        link: 'https://excelplay.org',
    },
    {
        id: '3',
        name: 'Algorithms',
        date: '05/02/2020',
        link: 'https://excelplay.org',
    },
    {
        id: '4',
        name: 'Algorithms',
        date: '05/02/2020',
        link: 'https://excelplay.org',
    }
]

const Home = () => {
    useEffect(() => {
        http.get('/values').then((res: any) => console.log(res));
    });
    return (
        <div className='content'>
            <div className='container-fluid'>
                <Suspense fallback={<DottedLineLoader />}>
                    <div className='row row-eq-height'>
                        <RegEvCard title={8} desc='Registered Events' icon='playlist_add_check' icon_bg='warning' />
                        <RegEvCard title={3} desc='Bookmarked Events' icon='book' icon_bg='info' />
                        <RegEvCard title={2} desc='Results' icon='thumb_up' icon_bg='success' />
                    </div>
                </Suspense>
                <Suspense fallback={<DottedLineLoader />}>
                    <div className='row'>
                        <div className='col-lg-6 col-md-12'>
                            <Table tableData={d1} tableTitle='Registered Events' />
                            <Table tableData={d2} tableTitle='Bookmarked Events' />
                        </div>
                        <div className='col-lg-6 col-md-12'>
                            <Table tableData={d3} tableTitle='Results' />
                        </div>
                    </div>
                </Suspense>
            </div>
        </div>
    );
};

export default Home;
