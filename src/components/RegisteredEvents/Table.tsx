import React from 'react';

interface TableRow {
    id: string;
    name: string;
    date: string;
    link: string;
}

interface TableProps {
    tableData: TableRow[];
    tableTitle: string
}

const Table = ({ tableData, tableTitle }: TableProps) => {
    const tableFields = Object.keys(tableData[0]).slice(0, -1);
    return (
        <div className='row' style={{ margin: '5px' }}>
            <div className='card' id={tableTitle}>
                <div className='card-header card-header-primary'>
                    <h4 className='card-title'>{tableTitle}</h4>
                    <p className='card-category'>New employees on 15th September, 2016</p>
                </div>
                <div className='card-body table-responsive'>
                    <table className='table table-hover'>
                        <thead>
                            {
                                tableFields.map((field, i) => (
                                    <th key={i}>{field.charAt(0).toUpperCase() + field.slice(1)}</th>
                                ))
                            }
                        </thead>
                        <tbody>
                            {
                                !!tableData.length && tableData.map((row: TableRow, i: number) =>
                                    <tr key={i}>
                                        <td>{row.id}</td>
                                        <td>{row.name}</td>
                                        <td>{row.date}</td>
                                        <td className='view-btn'>
                                            <a href={row.link} rel='noopener noreferrer' target='_blank'>
                                                <button className='btn btn-primary'>View</button>
                                            </a>
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default Table;