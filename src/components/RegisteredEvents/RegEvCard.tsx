import React from 'react';

interface RegEvProps {
    title: number;
    desc: string;
    icon: string;
    icon_bg: string;
}

const RegEvCard = ({ title, desc, icon, icon_bg }: RegEvProps) => {
    const handleClick = () => {
        try {
            document.getElementById(desc)!.scrollIntoView({ behavior: 'smooth' });
        }
        catch (error) {
            // couldn't scroll to position
        }
    }

    return (
        <div className='col-md-6 col-lg-4'>
            <div className='card card-stats reg-ev-card' onClick={handleClick}>
                <div className={`card-header card-header-${icon_bg} card-header-icon`}>
                    <div className='card-icon'>
                        <i className='material-icons'>{icon}</i>
                    </div>
                    <p className='card-category'>{desc}</p>
                    <h3 className='card-title'>{title}</h3>
                </div>
                <div className='card-footer'>
                  <div className='stats'>
                    <i className='material-icons'>date_range</i> Last 24 Hours
                  </div>
                </div>
            </div>
        </div>
    );
}

export default RegEvCard;