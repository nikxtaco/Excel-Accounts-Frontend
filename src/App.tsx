import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.scss';

import Base from './pages/Base';
import NotFound from './pages/NotFound/NotFound';

const App = () => {
  return (
    <Router>
      <Switch>
        <Route path='/' component={Base} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  )
};

export default App;
