import { ApiRoot } from './api';

interface postData {
  [key:string]: any;
}

const post = (url:string, data:postData) => {
  const headers:HeadersInit = new Headers();
  headers.append('Accept', 'application/json');
  headers.append('Content-Type', 'application/json');
  
  if (localStorage.getItem('jwt_token')) {
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('jwt_token'));
  }
  return fetch(ApiRoot + url, {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(data)
  })
    .then((res:any) => res.json())
    .catch((err:Error) => err);
};

const get = (url:string) => {
  const headers:HeadersInit = new Headers();
  headers.append('Accept', 'application/json');
  headers.append('Content-Type', 'application/json');

  if (localStorage.getItem('jwt_token')) {
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('jwt_token'));
  }
  return fetch(ApiRoot + url, {
    method: 'GET',
    headers: headers
  })
    .then((res:any) => res.json())
    .catch((err:Error) => console.log(err));
};

export default { post, get };